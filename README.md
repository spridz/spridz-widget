# Spridz.com Customer Survey Plugin Widget

This Wordpress plugin widget displays the Spridz customer survey.

It links through to your own Spridz customer survey hosted by spridz.com.

## Setup

1. Log in to spridz.com and go to the Integration page.
2. At the bottom of the Integration page find your API Token, copy this token.
3. On your Wordpress Admin page, go to Appearance, Widgets.
4. Find the Spridz Customer Survey widget, and press Add Widget.
5. Click to expand the Spridz Customer Survey widget, and paste the API Token
copied above into the API Token field.
6. Press the save button, then click done.


## Release Instructions

  git archive --prefix=spridz-widget/ --output=spridz-widget.zip HEAD

